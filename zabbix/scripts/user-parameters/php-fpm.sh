#!/bin/bash

function query() {
    once_flag=""
    if [ "$1" == "start-since" ]; then
        once_flag=" -m 1 ";
    fi
    php-fpm-status --socket=unix://$SOCKET --path=/-/status --full | sed -E 's/(^[a-z]*) ([a-zA-Z])/\1-\2/g' | sed -E 's/(^[a-z-]*) ([a-zA-Z])/\1-\2/g' | grep $once_flag "^$1:" | awk -F' ' '{ print $2}'
}

if [ $# == 0 ]; then
    echo $"Usage $0 {pool|process-manager|start-time|start-since|accepted-conn|listen-queue|max-listen-queue|listen-queue-len|idle-processes|active-processes|total-processes|max-active-processes|max-children-reached|slow-requests}"
    exit
else
    query "$1"
fi