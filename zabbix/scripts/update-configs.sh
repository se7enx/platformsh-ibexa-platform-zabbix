#!/usr/bin/env bash

ZABBIX_DIR=/app/zabbix
TEMP_BUILD_STORAGE=${ZABBIX_DIR}/tmp
CONFIGS_MOUNT=${ZABBIX_DIR}/configs

ZABBIX_DB_HOST=$(echo "$PLATFORM_RELATIONSHIPS" | base64 --decode | jq -r '.database_zabbix[0].host')
ZABBIX_DB_NAME=$(echo "$PLATFORM_RELATIONSHIPS" | base64 --decode | jq -r '.database_zabbix[0].path')
ZABBIX_DB_USER=$(echo "$PLATFORM_RELATIONSHIPS" | base64 --decode | jq -r '.database_zabbix[0].username')
ZABBIX_DB_PASS=$(echo "$PLATFORM_RELATIONSHIPS" | base64 --decode | jq -r '.database_zabbix[0].password')

# Zabbix Server
cp -f ${TEMP_BUILD_STORAGE}/zabbix_server.conf ${CONFIGS_MOUNT}/zabbix_server.conf
sed -i "/DBHost=/c\DBHost=$ZABBIX_DB_HOST" ${CONFIGS_MOUNT}/zabbix_server.conf
sed -i "/^DBName=/c\DBName=$ZABBIX_DB_NAME" ${CONFIGS_MOUNT}/zabbix_server.conf
sed -i "/^DBUser=/c\DBUser=$ZABBIX_DB_USER" ${CONFIGS_MOUNT}/zabbix_server.conf
sed -i "/DBPassword=/c\DBPassword=$ZABBIX_DB_PASS" ${CONFIGS_MOUNT}/zabbix_server.conf

# Web Interface Configs
mkdir -p ${CONFIGS_MOUNT}/ui
cp -f ${TEMP_BUILD_STORAGE}/ui/maintenance.inc.php ${CONFIGS_MOUNT}/ui/maintenance.inc.php
cp -f ${TEMP_BUILD_STORAGE}/ui/zabbix.conf.php.example ${CONFIGS_MOUNT}/ui/zabbix.conf.php
sed -i "/\$DB\['SERVER'\]/c\\\$DB['SERVER']='$ZABBIX_DB_HOST';" ${CONFIGS_MOUNT}/ui/zabbix.conf.php
sed -i "/\$DB\['DATABASE'\]/c\\\$DB['DATABASE']='$ZABBIX_DB_NAME';" ${CONFIGS_MOUNT}/ui/zabbix.conf.php
sed -i "/\$DB\['USER'\]/c\\\$DB['USER']='$ZABBIX_DB_USER';" ${CONFIGS_MOUNT}/ui/zabbix.conf.php
sed -i "/\$DB\['PASSWORD'\]/c\\\$DB['PASSWORD']='$ZABBIX_DB_PASS';" ${CONFIGS_MOUNT}/ui/zabbix.conf.php

# Zabbix Agent Templates
cp -f ${TEMP_BUILD_STORAGE}/zabbix_agentd.conf ${CONFIGS_MOUNT}/zabbix_agentd.conf
echo "Include=${ZABBIX_DIR}/templates/*.conf" >> ${CONFIGS_MOUNT}/zabbix_agentd.conf

DB_HOST=$(echo "$PLATFORM_RELATIONSHIPS" | base64 --decode | jq -r '.status_database[0].host')
DB_USER=$(echo "$PLATFORM_RELATIONSHIPS" | base64 --decode | jq -r '.status_database[0].username')
DB_PASS=$(echo "$PLATFORM_RELATIONSHIPS" | base64 --decode | jq -r '.status_database[0].password')

DB_CONFIG=${ZABBIX_DIR}/configs/.my.cnf
cp -f ${ZABBIX_DIR}/.my.cnf ${DB_CONFIG}
sed -i "s|DB_HOST|${DB_HOST}|g" "${DB_CONFIG}"
sed -i "s|DB_USER|${DB_USER}|g" "${DB_CONFIG}"
sed -i "s|DB_PASS|${DB_PASS}|g" "${DB_CONFIG}"